# C-Coding style

For C code we use kernel indentation.

## emacs

for emacs use the following config in your .emacs

```
(setq c-default-style "linux")
```
